import Vue from 'vue'
import App from './App'
import VueTouch from 'vue-touch'
import VueResource from 'vue-resource'
import HttpInterceptor from './httpInterceptor'

Vue.use(VueTouch);
Vue.use(VueResource);

Vue.http.interceptors.push(HttpInterceptor);

/* eslint-disable no-new */
new Vue({
  el: 'body',
  components: { App }
});
